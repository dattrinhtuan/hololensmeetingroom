﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using HoloToolkit.Unity;
using Vuforia;

public class AppManager : MonoBehaviour {
    public TextToSpeech tts;
    public GameObject uiElements;

    public static AppManager Instance
    {
        get;
        private set;
    }
    void Awake()
    {
        if (Instance == null){
            Instance = this;
        } else
            Destroy(this.gameObject);
    }

	// Use this for initialization
	void Start () {
        tts = GetComponent<TextToSpeech>();
        uiElements.SetActive(false);


        tts.StartSpeaking("Welcome to our meeting room booking application. Please take a look at a room sign to scan the room's name");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void LoadRoom(string room){
        uiElements.SetActive(true);
        uiElements.GetComponent<TapToPlaceExtension>().IsBeingPlaced = true;
        uiElements.GetComponent<Collider>().enabled = true;

        EnableSpatialMapping(true);
        tts.StartSpeaking(room + " is detected! Please look around, scan the environment and tap to place the calendar panel!");
        CalendarManager.Instance.ChangeRoom(room);
    }

    public void StartTracker(){
        uiElements.SetActive(false);
        ((TextTracker)TrackerManager.Instance.GetTracker<TextTracker>()).Start();

        tts.StartSpeaking("Please take a look at other room sign to scan the room's name again!");
    }

    public void EnableSpatialMapping(bool enabled)
    {
        if (enabled)
        {
            HoloToolkit.Unity.SpatialMapping.SpatialMappingManager.Instance.DrawVisualMeshes = true;
            HoloToolkit.Unity.SpatialMapping.SpatialMappingManager.Instance.StartObserver();
        } else {
            HoloToolkit.Unity.SpatialMapping.SpatialMappingManager.Instance.DrawVisualMeshes = false;
            HoloToolkit.Unity.SpatialMapping.SpatialMappingManager.Instance.StopObserver();
        }
    }
}
