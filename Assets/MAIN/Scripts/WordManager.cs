﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class WordManager : MonoBehaviour
{
    private TextTracker textTracker;

    void Start()
    {
        StartCoroutine(AddWord());
    }

    IEnumerator AddWord()
    {
        while ((TextTracker)TrackerManager.Instance.GetTracker<TextTracker>() == null)
        {
            yield return null;
        }

        textTracker = (TextTracker)TrackerManager.Instance.GetTracker<TextTracker>();
        textTracker.Stop();
        textTracker.WordList.SetFilterMode(WordFilterMode.WHITE_LIST);

        textTracker.WordList.AddWord("Meeting");
        textTracker.WordList.AddWordToFilterList("Meeting");

        textTracker.WordList.AddWord("Room");
        textTracker.WordList.AddWordToFilterList("Room");

        textTracker.WordList.AddWord("Hanoi");
        textTracker.WordList.AddWordToFilterList("Hanoi");

        textTracker.WordList.AddWord("Ha noi");
        textTracker.WordList.AddWordToFilterList("Ha noi");

        textTracker.WordList.AddWord("Hue");
        textTracker.WordList.AddWordToFilterList("Hue");

        textTracker.WordList.AddWord("Saigon");
        textTracker.WordList.AddWordToFilterList("Saigon");

        textTracker.WordList.AddWord("Sai gon");
        textTracker.WordList.AddWordToFilterList("Sai gon");

        Debug.Log("KEYWORDS ADDED!!!!!!!!!");
        textTracker.Start();
    }

    private void Update()
    {
        StateManager stateManager = TrackerManager.Instance.GetStateManager();
        IEnumerable<WordResult> wordResults = stateManager.GetWordManager().GetActiveWordResults();

        string sentence = "";
        foreach (WordResult wordResult in wordResults) // Find the patient name
        {
            sentence += wordResult.Word.StringValue;
        }

        sentence = sentence.ToLower();
        if (sentence.Contains("room") || sentence.Contains("meeting")){
            textTracker.Stop();

            if (sentence.Contains("hanoi") || sentence.Contains("ha noi"))
            {
                AppManager.Instance.LoadRoom(Booking.HANOI);   
            }
            else if (sentence.Contains("hue"))
            {
                AppManager.Instance.LoadRoom(Booking.HUE); 
            }
            else if (sentence.Contains("saigon") || sentence.Contains("sai gon"))
            {
                AppManager.Instance.LoadRoom(Booking.SAIGON); 
            }
        }
        
    }

}
