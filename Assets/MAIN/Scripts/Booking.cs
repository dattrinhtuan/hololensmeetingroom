﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Booking: MonoBehaviour {
    public static string HANOI = "Hanoi meeting room";
    public static string HUE = "Hue meeting room";
    public static string SAIGON = "Saigon meeting room";

    public Booking(){
        
    }
    public Booking(string room, DateTime date, float timeFrom, float timeTo, string person, string description)
    {
        Room = room;
        Date = date;
        TimeFrom = timeFrom;
        TimeTo = timeTo;
        Person = person;
        Description = description;
    }

    public string Room { get; set; }
    public DateTime Date { get; set; }
    public float TimeFrom { get; set; }
    public float TimeTo { get; set; }
    public string Person { get; set; }
    public string Description { get; set; }

    public void OnMeetingBookingClicked()
    {
        AppManager.Instance.tts.StartSpeaking(Person + " booked for " + Description);
    }
}
