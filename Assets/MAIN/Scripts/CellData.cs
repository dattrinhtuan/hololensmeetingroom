﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CellData : MonoBehaviour {
    public Sprite checkSprite;

    public DateTime Date { get; set; }
    public float Time { get; set; }

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClicked(){
        CalendarManager.Instance.OnCellClicked(this);
    }

    public void OnSelected(bool selected){
        if (selected)
            gameObject.GetComponent<Image>().sprite = checkSprite;
        else
            gameObject.GetComponent<Image>().sprite = null;
    }
}
