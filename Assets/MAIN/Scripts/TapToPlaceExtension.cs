﻿using System.Collections;
using System.Collections.Generic;
using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class TapToPlaceExtension : TapToPlace
{
    public override void OnInputClicked(InputClickedEventData eventData)
    {
        if (IsBeingPlaced)
        {
            base.OnInputClicked(eventData);

            GetComponent<Collider>().enabled = false;
            AppManager.Instance.EnableSpatialMapping(false);
        }
    }
}