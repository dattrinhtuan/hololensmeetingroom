using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CurvedUI;
using UnityEngine.Networking;

public class CalendarManager : MonoBehaviour
{
    public GameObject prefabCalendarCell;
    public GameObject prefabPanelBooking;
    public Canvas canvasCalendar;
    public Text textRoomTitle;

    public GameObject panelBookingForm;
    public Text textDate;
    public Dropdown dropdownPerson;
    public Dropdown dropdownDescription;


    public static float TIME_GAP = 1f;

    private static float START_TIME = 8; // 8AM
    private static float END_TIME = 18; // 18PM
    private static int NUMBER_OF_COLUMNS = (int)((END_TIME - START_TIME - TIME_GAP)/TIME_GAP) + 2;
    private static int NUMBER_OF_DAY_DISPLAYED = 3 * 2 + 1; // Should be an odd number
    private static bool ROW_COLOR = false;

    private String DATE_FORMAT = "dd/MM/yyyy";
    private static int GAP = 2;
    private int rowIndex = 0;
    private Dictionary<String, GameObject> cellDictionary;
    private bool cached;
    private SelectionData selectionData;
    private DateTime currentDate;
    private String currentRoom;
    private List<Booking> bookings;

    public static CalendarManager Instance
    {
        get;
        private set;
    }
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
            Destroy(this.gameObject);
    }




    public void OnCellClicked(CellData cd)
    {
        UpdateCellsSelected(false);
        selectionData.Add(cd.Date, cd.Time);
        UpdateCellsSelected(true);

        if (selectionData.NothingSelected)
        {
            panelBookingForm.SetActive(false);
        }
        else
        {
            panelBookingForm.SetActive(true);
            textDate.text = ConvertTimeFloatToString(selectionData.TimeFrom) +
                " To " + ConvertTimeFloatToString(selectionData.TimeTo) +
                ", " + selectionData.Date.ToString(DATE_FORMAT);
        }
    }
    private void UpdateCellsSelected(bool selected)
    {
        for (float f = selectionData.TimeFrom; f < selectionData.TimeTo; f += TIME_GAP)
        {
            GameObject cell;
            cellDictionary.TryGetValue(selectionData.Date.ToString(DATE_FORMAT) + f, out cell);
            cell.GetComponent<CellData>().OnSelected(selected);
        }
    }




    #region to be called for UI controls event handler
    /// Shift current displayed day
    public void ShiftDay(int day)
    {
        ChangeDate(currentDate.AddDays(day));
    }
    public void ChangeCanvasAngle(Slider slider)
    {
        canvasCalendar.GetComponent<CurvedUISettings>().Angle = (int)slider.value;
    }
    public void ChangeMonth(Dropdown dropdown)
    {
        DateTime date = new DateTime(DateTime.Now.Year, dropdown.value + 1, 1);
        ChangeDate(date);
    }
    public void SubmitBookingForm()
    {
        if (selectionData.NothingSelected)
        {
            return;
        }

        StartCoroutine(SendBookingFormRequest());
        bookings.Add(new Booking(currentRoom, selectionData.Date, selectionData.TimeFrom, selectionData.TimeTo, dropdownPerson.options[dropdownPerson.value].text, dropdownDescription.options[dropdownPerson.value].text));

        UpdateCellsSelected(false); // Let selected cells go to normal state
        AddBooking(selectionData.Date, selectionData.TimeFrom, selectionData.TimeTo, dropdownPerson.options[dropdownPerson.value].text, dropdownDescription.options[dropdownPerson.value].text);

        selectionData.Reset();
        panelBookingForm.SetActive(false);
    }
    public void ChangeRoom(String room){
        currentRoom = room;
        textRoomTitle.text = room;

        if (cached)
        {
            ChangeDate(DateTime.Now);
        } else {
            StartCoroutine(FetchDataFromGoogleSpreadsheet());
        }
    }
    #endregion


    // Use this for initialization
    void Start()
    {
        bookings = new List<Booking>();
        cellDictionary = new Dictionary<string, GameObject>();
        cached = false;
        currentRoom = Booking.HANOI;

        panelBookingForm.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }




    private void ChangeDate(DateTime date)
    {
        // Destroy all previously created game objects
        foreach (Transform tf in transform)
        {
            Destroy(tf.gameObject);
        }

        selectionData = new SelectionData();

        currentDate = date;
        this.GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 0f);
        rowIndex = 0;
        cellDictionary = new Dictionary<string, GameObject>();

        Rect rect = prefabCalendarCell.GetComponent<RectTransform>().rect;
        float width = (rect.width + GAP) * (NUMBER_OF_COLUMNS);
        float height = (rect.height + GAP) * NUMBER_OF_DAY_DISPLAYED;
        this.GetComponent<RectTransform>().sizeDelta = new Vector2(width, height);

        DateTime beginDate = date.AddDays(-(NUMBER_OF_DAY_DISPLAYED - 1) / 2);
        for (int i = 0; i < NUMBER_OF_DAY_DISPLAYED; ++i)
        {
            AddRow(beginDate);
            beginDate = beginDate.AddDays(1);
        }

        foreach (Booking booking in bookings)
        {
            if (currentRoom.Equals(booking.Room) && Math.Abs((date - booking.Date).Days) <= (NUMBER_OF_DAY_DISPLAYED - 1) / 2)
            {
                AddBooking(booking.Date, booking.TimeFrom, booking.TimeTo, booking.Person, booking.Description);
            }
        }
    }

    private void AddRow(Nullable<DateTime> date)
    {
        Rect rect = prefabCalendarCell.GetComponent<RectTransform>().rect;
        float top = -(rect.height + GAP) * rowIndex;

        for (int i = 0; i < NUMBER_OF_COLUMNS; ++i)
        {

            GameObject cell = Instantiate(prefabCalendarCell);
            cell.SetActive(true);
            cell.transform.SetParent(this.gameObject.transform, false);
            RectTransform crt = cell.GetComponent<RectTransform>();
            crt.localPosition += new Vector3((crt.rect.width + GAP) * i, top, 0);


            Text text = cell.GetComponentInChildren<Text>();
            float time = START_TIME + TIME_GAP * (i - 1);
            String timeText = ConvertTimeFloatToString(time);

            if (i == 0)
            { // Row Header
                text.text = date.Value.ToString(DATE_FORMAT) + "\n" + date.Value.DayOfWeek;

                cell.GetComponent<Image>().color = Color.black;
                text.color = Color.white;
            }
            else
            { // Normal cell
                text.text = timeText + "\n" + date.Value.ToString(DATE_FORMAT);

                if (ROW_COLOR){
                    if (rowIndex % 2 != 0)
                        cell.GetComponent<Image>().color = new Color32(248, 251, 188, 150);
                    else
                        cell.GetComponent<Image>().color = new Color32(161, 97, 184, 150);
                }
                cellDictionary.Add(date.Value.ToString(DATE_FORMAT) + time, cell);
            }

            CellData cd = cell.GetComponent<CellData>();
            cd.Date = date.Value;
            cd.Time = time;

            cell.name = date.Value.ToString(DATE_FORMAT) + time;
        }
        rowIndex++;
    }


    private void AddBooking(DateTime date, float timeFrom, float timeTo, string person, string description)
    {
        GameObject cellStart;
        cellDictionary.TryGetValue(date.ToString(DATE_FORMAT) + timeFrom, out cellStart);

        if (cellStart == null) // Sometime, the days different should be n, but it is round to n+1
            return;


        // Remove default text inside
        for (float f = timeFrom; f < timeTo; f += TIME_GAP)
        {
            GameObject cell;
            cellDictionary.TryGetValue(date.ToString(DATE_FORMAT) + f, out cell);
            Text text = cell.GetComponentInChildren<Text>();
            text.text = "";
        }


        // Add blank panel
        GameObject panelBooking = Instantiate(prefabPanelBooking);
        panelBooking.SetActive(true);
        panelBooking.transform.SetParent(this.gameObject.transform, false);
        RectTransform crt = panelBooking.GetComponent<RectTransform>();
        crt.localPosition = new Vector3(cellStart.GetComponent<RectTransform>().localPosition.x + GAP,
                                         cellStart.GetComponent<RectTransform>().localPosition.y - GAP,
                                         0);
        Rect rect = prefabCalendarCell.GetComponent<RectTransform>().rect;
        float width = (timeTo - timeFrom) / TIME_GAP * rect.width
                                         - 2 * GAP // beginning & end gap
                                         + (timeTo - timeFrom) / TIME_GAP * GAP; // gap between cells
        float height = rect.height - 2 * GAP;
        crt.GetComponent<RectTransform>().sizeDelta = new Vector2(width, height);


        Booking booking = prefabPanelBooking.GetComponent<Booking>();
        booking.Description = description;
        booking.Person = person;


        // Update txt position
        Text txt = panelBooking.GetComponentInChildren<Text>();
        txt.GetComponent<RectTransform>().localPosition = new Vector3(GAP, -GAP, 0);
        txt.GetComponent<RectTransform>().sizeDelta = new Vector2(width - 2 * GAP, height - 2 * GAP);



        //Update txt content
        txt.text = GetShortStringToBeDisplayed(ConvertTimeFloatToString(timeFrom) + "-" + ConvertTimeFloatToString(timeTo) + "\n" +
                                               person + ": " + description, (int)((timeTo - timeFrom)/TIME_GAP) + 1);
    }

    private String GetShortStringToBeDisplayed(String input, int factor)
    {
        int SIZE = 30 * factor;
        if (input.Length <= SIZE)
        {
            return input;
        }
        else
        {
            return input.Substring(0, SIZE) + "...";
        }
    }

    private String ConvertTimeFloatToString(float time)
    {
        float hour = Mathf.Floor(time);
        if (time - hour > 0)
        {
            return hour + ":30";
        }
        else
            return hour + ":00";

    }





    #region Google Interaction
    private IEnumerator FetchDataFromGoogleSpreadsheet()
    {
        // Step 1: send request to get the data
        string url = "https://docs.google.com/spreadsheets/d/e/2PACX-1vTZ7W66rCFQJi06lSY0dn3QYy89aQcpZYBmPHyCWI6YLfIo06eLyELL74AY0VEwNxOFWLZRlqrgij60/pub?output=csv";
        string text;

        UnityWebRequest uwr = UnityWebRequest.Get(url);
        yield return uwr.SendWebRequest();
        if (uwr.isNetworkError)
        {
            Debug.Log("Error While Sending: " + uwr.error);
        }

        text = uwr.downloadHandler.text;
        bookings = new List<Booking>();

        string[] rows = text.Split('\n');
        string[] cells;

        for (int i = 1; i < rows.Length; ++i)
        {
            cells = rows[i].Split(',');

            float timeFrom, timeTo;
            DateTime dt = DateTime.Parse(cells[2]);
            if (dt.Minute > 0)
                timeFrom = dt.Hour + TIME_GAP;
            else
                timeFrom = dt.Hour;
            dt = DateTime.Parse(cells[3]);
            if (dt.Minute > 0)
                timeTo = dt.Hour + TIME_GAP;
            else
                timeTo = dt.Hour;

            dt = DateTime.Parse(cells[4]);

            bookings.Add(new Booking(cells[1], dt, timeFrom, timeTo, cells[5], cells[6]));
        }

        cached = true;
        ChangeDate(DateTime.Now);
    }
    private IEnumerator SendBookingFormRequest()
    {
        string url = "https://docs.google.com/forms/d/e/1FAIpQLSe_zr0emUm_LdiuxBeJvwRcMOHQTE4UdY_dXHScdJ7j-HG2fg/formResponse";
        WWWForm form = new WWWForm();
        form.AddField("entry.569227169", currentRoom);
        form.AddField("entry.1306293867", ConvertTimeFloatToString(selectionData.TimeFrom));
        form.AddField("entry.409286955", ConvertTimeFloatToString(selectionData.TimeTo));
        form.AddField("entry.1329237071", selectionData.Date.ToString("MM/dd/yyyy"));
        form.AddField("entry.673964126", dropdownPerson.options[dropdownPerson.value].text);
        form.AddField("entry.1391649195", dropdownDescription.options[dropdownPerson.value].text);

        UnityWebRequest www = UnityWebRequest.Post(url, form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Form upload complete!");
        }
    }
    #endregion
}
