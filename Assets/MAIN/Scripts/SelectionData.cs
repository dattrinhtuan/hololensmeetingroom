﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionData {
    public bool NothingSelected { get; set; }
    public DateTime Date { get; set; }
    public float TimeFrom { get; set; }
    public float TimeTo { get; set; }

    public SelectionData(){
        Reset();
    }

    public void Reset(){
        TimeFrom = -1;
        TimeTo = -1;
        NothingSelected = true;
    }

    public void Add(DateTime date, float time){
        NothingSelected = false;
        if (NothingSelected || (date-Date).Days != 0) {
            AddSingleSelection(date, time);
        } else {
            if (TimeFrom - time > CalendarManager.TIME_GAP || time - TimeTo >= CalendarManager.TIME_GAP) {
                AddSingleSelection(date, time);
            } else if (TimeFrom.Equals(time +CalendarManager.TIME_GAP)){
                TimeFrom = time;
            } else if (TimeTo.Equals(time)) {
                TimeTo = time + CalendarManager.TIME_GAP;
            } else if ((TimeTo - TimeFrom).Equals(CalendarManager.TIME_GAP) && time.Equals(TimeFrom)){ // Previously one cell selected, and now we deselect it
                Reset();
            } else if (time.Equals(TimeFrom)){
                TimeFrom = time + CalendarManager.TIME_GAP;
            } else if (time.Equals(TimeTo - CalendarManager.TIME_GAP)){
                TimeTo = time;
            } else {
                TimeTo = time + CalendarManager.TIME_GAP;
            }
        }
    }

    public void AddSingleSelection(DateTime date, float time) {
        Date = date;
        TimeFrom = time;
        TimeTo = time + CalendarManager.TIME_GAP;
    }
}
